resource "aws_security_group" "myapp-sg" {

    vpc_id = var.vpc_id
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = [var.my-ip]
    }
    ingress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]

    }
    egress {
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
        prefix_list_ids = []

    }
    tags = {
        Name = "${var.env_prefix}-sg"
    }
}

data "aws_ami" "latest-ami" {
    most_recent = true
    owners = ["amazon"]
    filter {
        name = "name"
        values = ["amzn2-ami-hvm-*-x86_64-gp2"]
    }
    filter {
        name = "virtualization-type"
        values = ["hvm"]
    }
}




resource "aws_key_pair" "ssh-key" {
    key_name = "${var.env_prefix}-server-key"
    public_key = "${file(var.public_key_location)}"
}

resource "aws_instance" "myapp-server" {

    count =1
    ami = data.aws_ami.latest-ami.id
    instance_type = "t2.micro"
    subnet_id = var.subnet_id
    vpc_security_group_ids = [aws_security_group.myapp-sg.id]
    availability_zone = var.a-zone
    associate_public_ip_address = true
    key_name = aws_key_pair.ssh-key.key_name
    # user_data = file("entry-script.sh")

   connection {
       type = "ssh"
       host = self.public_ip
       user = "ec2-user"
       private_key = file(var.private_key_location)
   }

    provisioner "remote-exec" {
        inline = [
            "export ENV=dev",
            "mkdir kalhaar"
        ]
    
    }

    tags = {
        Name = "${var.env_prefix}-server-${count.index+1}"
    }

    provisioner "local-exec" {

        # working_dir = "/home/savaj/ansible-learning"
        command = "ansible-playbook --inventory ${self.public_ip}, --private-key ${var.private_key_location} --user ec2-user deploy-docker.yaml"
    
    }
    


}

