


provider "aws" {
    region = "us-east-1"
}


module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "my-vpc"
  cidr = var.vpc_cidr_block

  azs             = [var.a-zone]
  public_subnets  = [var.subnet_cidr_block]
  public_subnet_tags = {
      Name = "${var.env_prefix}-subnet-1"
  }

  tags = {
    Name = "${var.env_prefix}-vpc"
  }
}

module "myapp-webserver" {
    source = "./modules/webserver"
    a-zone = var.a-zone
    env_prefix = var.env_prefix
    vpc_id = module.vpc.vpc_id   
    my-ip = var.my-ip
    public_key_location = var.public_key_location
    instance_type = "t2.micro"
    subnet_id = module.vpc.public_subnets[0]
    private_key_location = var.private_key_location
    
    
}

